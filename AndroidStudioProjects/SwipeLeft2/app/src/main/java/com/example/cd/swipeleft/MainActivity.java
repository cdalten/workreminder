package com.example.cd.swipeleft;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button login;
    private Button exit;
    private Button screen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = (Button)findViewById(R.id.button);
        exit = (Button)findViewById(R.id.button2);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //exit.setText("Logged Out.");
                SwipeLeft(v);
            }
        });

    }

    public void SwipeLeft(View view) {
        //exit.setText("Permission Denied.");
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit.setText("Logged Out.");
            }
        });

    }

    //Doesn't take View
    public void SwipeRight() {

    }
}//end MainActivity
