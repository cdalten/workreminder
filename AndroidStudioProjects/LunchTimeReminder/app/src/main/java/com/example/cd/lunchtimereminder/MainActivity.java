package com.example.cd.lunchtimereminder;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity {

    public static String WORK_URL = "usr56.dayforcehcm.com/mydayforce/mydayforce.aspx";
    private static String DEBUG_TAG = "LG_WORK_PHONE";
    private WebView getSchedule; //Added on 2 - 3- 2020

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadWebPage("https://" + WORK_URL);
    }


    //Added on 2 - 4- 2020
    public void loadJSON() {

    }

    //Added on 2 - 2 - 2020
    public void loadWebPage(String url) {
        ConnectivityManager cm =
                (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();


        getSchedule = (WebView) this.findViewById(R.id.CurrentSchedule);
        getSchedule.setWebViewClient(new WWebViewClient());
        //getSchedule.addJavascriptInterface(new MainActivity.JavaScriptBridge(this), "OFFLINE");
        getSchedule.getSettings().setLoadWithOverviewMode(true);
        getSchedule.getSettings().setUseWideViewPort(true);
        getSchedule.getSettings().setJavaScriptEnabled(true);
        getSchedule.getSettings().setDomStorageEnabled(true);
        getSchedule.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT); //added on 9 - 23 - 2018
        CookieManager.getInstance().setAcceptCookie(true);



        //mNetworkFragment = WorkNetworkFragment.getInstance(
        //        getSupportFragmentManager(),
        //        LOGIN_URL);

        getSchedule.loadUrl(url);
        getSchedule.setVisibility(View.VISIBLE); //disable for debugging.

    }

    @TargetApi(21)
    public class WWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            Log.e(DEBUG_TAG, "ON PAGE STARTED---------------------------------------------------");
            Log.e(DEBUG_TAG, "THE ORIGINAL VIEW URL IS: " + view.getOriginalUrl());
            Log.e(DEBUG_TAG, "THE VIEW URL IS: " + view.getUrl());
            Log.e(DEBUG_TAG, "THE VIEW TITLE IS: " + view.getTitle());
            Log.e(DEBUG_TAG, "ON PAGE STARTED----------------------------------------------------");
        }

        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            Log.e(DEBUG_TAG, "----------------------------------------------------");
            Log.e(DEBUG_TAG, "THE REQUEST URL IS: " + request.getUrl());
            Log.e(DEBUG_TAG, "THE REQUEST METHOD IS: " + request.getMethod());
            Log.e(DEBUG_TAG, "THE REQUEST ENCODED PATH IS: " + request.getUrl().getEncodedPath());
            Log.e(DEBUG_TAG, "THE REQUEST LAST PATH IS: " + request.getUrl().getLastPathSegment());
            Log.e(DEBUG_TAG, "THE REQUEST HOST IS: " + request.getUrl().getHost());
            Log.e(DEBUG_TAG, "THE REQUEST AUTHORITY IS: " + request.getUrl().getAuthority());
            Log.e(DEBUG_TAG, "THE REQUEST ENCODED AUTHORITY IS: " + request.getUrl().getEncodedAuthority());
            Log.e(DEBUG_TAG, "THE REQUEST ENCODED QUERY IS: " + request.getUrl().getEncodedQuery());
            Log.e(DEBUG_TAG, "THE REQUEST ENCODED USER INFO IS: " + request.getUrl().getEncodedUserInfo());
            Log.e(DEBUG_TAG, "THE REQUEST FRAGMENT INFO IS: " + request.getUrl().getFragment());
            Log.e(DEBUG_TAG, "THE REQUEST HEADERS ARE IS: " + request.getRequestHeaders());
            //--------------------------------------------------------------------------------------


            Log.e(DEBUG_TAG, "----------------------------------------------------");

            return super.shouldInterceptRequest(view, request);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            //return super.shouldOverrideUrlLoading(view, request);
            if ("usr57.dayforcehcm.com".equals(Uri.parse(request.getUrl().toString()).getHost())) {
                // This is my website, so do not override; let my WebView load the page
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(request.getUrl().toString()));
            startActivity(intent);
            return true;

        }

        @Override
        public void onPageFinished(WebView view, String url){
            //Check for url, then call loadWebPage to load log files from shouldIntercept()
            //loadWebPage("file:///android_asset/currentLog.html");
            Log.e(DEBUG_TAG, "ON PAGE FINISHED---------------------------------------------------");
            Log.e(DEBUG_TAG, "THE ORIGINAL VIEW URL IS: " + view.getOriginalUrl());
            Log.e(DEBUG_TAG, "THE VIEW URL IS: " + view.getUrl());
            Log.e(DEBUG_TAG, "THE VIEW TITLE IS: " + view.getTitle());
            Log.e(DEBUG_TAG, "ON PAGE FINISHED----------------------------------------------------");
            String cookies = CookieManager.getInstance().getCookie(url);
            Log.e(DEBUG_TAG, "All the cookies in a string:" + cookies);
            writeToFile(cookies);

        }//end for
    }//end WebViewClient class

    //Added on 2 - 6 - 2020. Ripped off from Stackoverflow
    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("currentLog.html", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
}//end class
