package com.example.cd.homelessfoodmapapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;
    LocationRequest locationRequest;
    private final static String DEBUG_TAG = "LG_PHONE"; //added on 7 - 20 - 2019

    private static final int PERMISSION_REQUEST_LOCATION = 1; //Added on 7 - 22 - 2019

    private static double lat; //Added on 7 - 23 - 2019
    private static double lng; //Added on 7 - 23 - 2019

    private Geocoder geocoder; //Added on 7 - 23 - 2019
    private List possibleAddressMatches; //Added on 7 - 23 - 2019

    protected Location lastLocation;
    private AddressResultReceiver resultReceiver;
    private final int REQUEST_CHECK_SETTINGS = 1; //Added on 7 - 30 - 2019

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        Log.e(DEBUG_TAG, "IS SAVED INSTANCE STATE NULL? " + savedInstanceState);
        //startActivity(new Intent(MapsActivity.this, ServiceCalendar.class
        //));

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


        //ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        createLocationCallback();
        createLocationRequest();

        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSION_REQUEST_LOCATION);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        return permissionState == PackageManager.PERMISSION_GRANTED;

    }

    //Added and modified on 7 - 30 - 2019
    protected void createLocationRequest() {
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    //Do I need to call super??"
    protected void createLocationCallback() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult == null) {
                    return;
                }

                for (Location location : locationResult.getLocations()) {
                    Log.i(DEBUG_TAG, "CURRENT LAT: " + location.getLatitude());
                    Log.i(DEBUG_TAG, "CURRENT LNG: " + location.getLongitude());
                    //startLocationUpdates();
                    // Update UI with location data
                    // ...
                }

                lastLocation = locationResult.getLastLocation(); //pass to intent
                Log.e(DEBUG_TAG, "THE LAST LOCATION BEFORE THE INTENT IS: " + lastLocation);
                startIntentService();
                updateUI();
            };
        };
    }


    //Added on 7 - 28 - 2019
    private void updateUI() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        LatLng currentLocation = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(currentLocation).title("No hoochie mammas"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));

        float zoomLevel = 16.0f; //This goes up to 21
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, zoomLevel));
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    //Added on 7 - 23 - 2019
    /*@Override
    protected void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
        outState.putDouble("LAT", lat);
        outState.putDouble("LNG", lng);
        super.onSaveInstanceState(outState);
    }
    */


    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(DEBUG_TAG, "ON RESUME GOT CALLED");
        //if (requestingLocationUpdates) {
        if (checkPermissions()) {
            startLocationUpdates();
        }
        //}
    }

    private void startLocationUpdates() {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            SettingsClient client = LocationServices.getSettingsClient(this);

            Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

            task.addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                @Override
                public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                    // All location settings are satisfied. The client can initialize
                    // location requests here.
                    // ...
                    Log.e(DEBUG_TAG, "IS LOCATION PRESENT?: " + locationSettingsResponse.getLocationSettingsStates().isLocationPresent());
                    Log.e(DEBUG_TAG, "IS GPS PRESENT?: " + locationSettingsResponse.getLocationSettingsStates().isGpsPresent());
                    fusedLocationClient.requestLocationUpdates(locationRequest,
                            locationCallback,
                            Looper.myLooper() /* Looper */);

                    //Debugging only. Need to remove..
//                    Log.e(DEBUG_TAG, "THE NEW LATITUDE IS: " + lastLocation.getLatitude() );
//                    Log.e(DEBUG_TAG, "THE NEW LNG IS: " + lastLocation.getLongitude());
                    updateUI();
                }
            });

            task.addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    if (e instanceof ResolvableApiException) {
                        // Location settings are not satisfied, but this can be fixed
                        // by showing the user a dialog.
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult(MapsActivity.this,
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException sendEx) {
                            // Ignore the error.
                        }
                    }
                }
            });
        }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        // BEGIN_INCLUDE(onRequestPermissionsResult)
        if (requestCode == PERMISSION_REQUEST_LOCATION) {
            // Request for camera permission.
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission has been granted. Start camera preview Activity.
                Log.i(DEBUG_TAG, "PERMISSION GRANTED");

            } else {
                // Permission request was denied.
                Log.i(DEBUG_TAG, "PERMISSION DENIED");
            }
        }
        // END_INCLUDE(onRequestPermissionsResult)
    }

    protected void startIntentService() {
        Intent intent = new Intent(this, FetchAddressIntentService.class);
        resultReceiver = new AddressResultReceiver(new Handler());
        intent.putExtra(Constants.RECEIVER, resultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, lastLocation);
        startService(intent);
    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultData == null) {
                return;
            }

            // Display the address string
            // or an error message sent from the intent service.
            String addressOutput = resultData.getString(Constants.RESULT_DATA_KEY);
            lat = Double.parseDouble(resultData.getString("lat"));
            lng = Double.parseDouble(resultData.getString("lng"));
            if (addressOutput == null) {
                addressOutput = "";
            }

            geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
            //try {
                //possibleAddressMatches = geocoder.getFromLocationName("555 California Street, San Francisco, CA", 5);
                //possibleAddressMatches = geocoder.getFromLocationName(addressOutput, 5);
                //Address myAddress = (Address)possibleAddressMatches.get(0);
                //lat = myAddress.getLatitude();
                //lng = myAddress.getLongitude();

            //} catch (IOException e) {
                //pass
            //}
            //displayAddressOutput();

            // Show a toast message if an address was found.
            if (resultCode == Constants.SUCCESS_RESULT) {
                //showToast(getString(R.string.address_found));
                Log.i(DEBUG_TAG, "ADDRESS FOUND");
            }

            updateUI();

        }
    }
}//end class
