package com.example.cd.runningipc;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class PipedActivity extends AppCompatActivity{
    private String PRODUCTION_TAG = "LG_TEST_PHONE";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("START_MINUTE", "4"); //force int to string
        startActivity(i);
        Log.i(PRODUCTION_TAG, "PIPED ACTIVITY INTENT GOT CALLED");
        finish();
    }


}
