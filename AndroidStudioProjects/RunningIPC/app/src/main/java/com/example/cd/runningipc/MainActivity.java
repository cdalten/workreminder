package com.example.cd.runningipc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private String PRODUCTION_TAG = "LG_TEST_PHONE";
    public static String htmlOut= "<html>SAVED HMTL </html>"; //Added on 2 - 4 - 2019
    public static final String URL_KEY = "URL_KEY"; //Adde on 2 - 4 - 2019

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i(PRODUCTION_TAG, "DEBUGGER STOPPED"); //force debugger to stop
        Intent i=new Intent(this,PipedActivity.class);
        //i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        //i.putExtra("START_MINUTE","4");//goes to previous INtent
        startActivity(i);//will trigger only myMethod in MainActivity
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(intent.getStringExtra("START_MINUTE").equals("4")) {
            Log.i(PRODUCTION_TAG, "MAIN ACTIVITY NEW INTENT IS:  " + intent.getStringExtra("START_MINUTE"));
        } else {
            Log.i(PRODUCTION_TAG, "THE MAIN ACTIVITY NEW INTENT IS NOT SET");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(URL_KEY, htmlOut);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        savedInstanceState.get(URL_KEY);
    }
}//end class
