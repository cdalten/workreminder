package com.example.cd.shiftreminder;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.icu.util.Calendar;
//import android.support.design.widget.AppBarLayout;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.http.HttpResponseCache;
import android.os.Build;
import android.os.Message;
import android.provider.AlarmClock;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.HttpAuthHandler;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TabHost;
import android.widget.Toast;

//import com.example.cd.shiftreminder.cacheURL;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

/*
 * TODO:
 * 1)Load progress bar?
 * 2)Check for url fail only when a network connection is established.
 * 3)App crashed when loaded w/o a connection. Related to no getContext()? If so, where do
 *   I put this method at and why?
 .
 *
 */
public class MainActivity extends AppCompatActivity {
    public WebView getSchedule;
    //protected static String STRIPPED_URL = "myschedule.safeway.com";
    public static String STRIPPED_URL = "myschedule.safeway.com/ESS/AuthN/SwyLogin.aspx?ReturnUrl=%2fESS";
    //protected String TEST_URL = "https://groups.google.com/#!forums/comp.lang.c";
    protected static final String UA = "Pak N Slave Mobile App; Written by cda@stanford.edu; Uhh...Hi Mom!";
    //private final String UA = "Mozilla/5.0(Windows NT 10.0;Win64;x64) AppleWebKit/537.36;(KHTML," +
    //        "like Gecko) Chrome/52.0.2743.116 Safari/537.36 Edge/15.15063"; //Prevent redirection?!
    private String WORK_LOGIN_URL;

    //private TabLayout.Tab alarm; //added on 9 - 14 - 2017
    private TabHost alarm; //added on 9 - 14 - 2017
    protected Context context; // added on 8 - 22 - 2017
    protected File cacheDir; //added on 8 - 30 - 2017
    protected String longinURL; //added on 9 - 9 - 2017
    protected String filename; //added on 9 - 21 - 2017
    protected File file; //added on 9 - 21 - 2017
    protected TabLayout th; //added on 9 - 28 - 2017

    private int log; //added on 10 - 30 - 2017. Need to remove

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSchedule = new WebView(this); //works
        //setContentView(R.layout.activity_main);
        setContentView(getSchedule);//works

        findMax(4,5);
        //getSchedule = (WebView) findViewById(R.id.WebView); //modified on 10 -3 - 2017
        //getSchedule.setWebViewClient(new WorkWebViewClient());
        //getSchedule.setWebViewClient(new WebViewClient());
        getSchedule.getSettings().setDomStorageEnabled(true); //added on 10 - 5 - 2017
        getSchedule.getSettings().setDatabaseEnabled(true); //added on 10 - 5- 2017

        /*
         * Add find view before savedInstanceState and then check for NOT null. Why?
         */
        /*if (savedInstanceState != null) {
            //savedInstanceState.getBundle("URL");
            savedInstanceState.getString("URL", "https://" + "" + STRIPPED_URL);
        }
        */
        /*if (savedInstanceState != null) {
            savedInstanceState.getBundle("URL");
        }
        */

        //setAlarm(); //added on 9 - 13 - 2017
        /*th = (TabLayout) findViewById(R.id.tabs);
        th.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                if (pos == 0) {
                    setAlarm();
                } else if (pos == 1) {
                    setTextReminder();
                }
            }
        });

        */

        //alarm = (TabLayout.Tab) findViewById(R.id.alarm);

        WebView.setWebContentsDebuggingEnabled(true);

        //getSchedule.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath(
        getSchedule.getSettings().setLoadWithOverviewMode(true);
        getSchedule.getSettings().setUseWideViewPort(true);
        //getSchedule.getSettings().setAppCacheEnabled(false); //?? Changed on 10 - 12 - 2017
        getSchedule.getSettings().setJavaScriptEnabled(true);
        getSchedule.getSettings().setUserAgentString(UA);
        //getSchedule.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE); //Changed on 10 -12 - 2017

        /*
         * Added beta auto login sequence on 10 - 10 - 2017
         */
        //CookieManager autoLogin = CookieManager.getInstance();
        //autoLogin.acceptCookie();

        getSchedule.loadUrl("https://" + STRIPPED_URL);
        getSchedule.setWebViewClient(new WorkWebViewClient());
        //getSchedule.setWebViewClient(new WebViewClient());
        getSchedule.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                //return super.onConsoleMessage(consoleMessage);
                Log.d("work", consoleMessage.message() + " --- From line"
                + consoleMessage.lineNumber() + " of" +
                consoleMessage.sourceId());
                return true;
            }
        }); //Handle JQuery dialog box?
        //getSchedule.loadUrl("https://myschedule.safeway.com");
        //getSchedule.loadUrl("javascript:document.getElementsByName('EmpID').value = '9857701'");
        /*
         *I need to copy the html file to a temp file since I can't cache
         * on a post request.
         */
        //cacheURL(this, STRIPPED_URL);
        try {
            File httpCacheDir = new File(this.getCacheDir(), "http");
            long httpCacheSize = 10 * 1024 * 1024; // 10 MiB
            HttpResponseCache.install(httpCacheDir, httpCacheSize);
        } catch (IOException e) {
            //Log.i(TAG, "HTTP response cache installation failed:" + e);
        }


    }

    //need to erase from change logs 10 - 30 - 2017
    public void findMax(int a, int b) {
        int temp;
        int x = 1;

        temp = a;
        a = b;
        b = temp;

        log = temp;
        log = a;
        x = 5; //trigger
    }

    /*@Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //getSchedule.saveState(outState);
        //outState.putBundle("URL", saveUrlState());
        outState.putString("URL", STRIPPED_URL);
    }
    */

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //getSchedule.restoreState(savedInstanceState);
    }


    @Override
    protected void onStop() {
        super.onStop();

        HttpResponseCache cache = HttpResponseCache.getInstalled();

        if (cache != null) {
            cache.flush();
        }//end if
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected Bundle saveUrlState(){
        Bundle map = new Bundle();
        map.putString("URL", "https://" + "" + STRIPPED_URL);
        return map;
    }
    /*
      * Possibly change to private to "File cacheURL()" and then have it return type File. 9 - 14 - 2017
       * Right now I have it as a file. Why the difference? 9 - 21 - 2017
       *
     */
    private void cacheURL(Context context, String url) {
        file = null;

        try {
            filename = Uri.parse(url).getLastPathSegment();
            //String filename = Uri.parse(url).toString();

            cacheDir = context.getCacheDir();
            if (!cacheDir.exists()) {
                cacheDir.mkdirs();
            } else {
                file = File.createTempFile(filename, null, cacheDir);
            }
        } catch (Exception e) {

        }

        if (!isConnected(context)) {
            //getSchedule.loadUrl("file://" + file.getPath()); //load cache
        } else {
            //getSchedule.loadUrl("file://" + file.getAbsolutePath());
            //getSchedule.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
        }
    }//end cacheURL

     /*
         * Attempt to cache non mobile site for offline browsing. Untested.
    */
    private boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        /*if (ni != null) {
            return true;
        } else if (ni.isAvailable()) {
            return true;
        } else {
            return false; //attempt to call cached link?
        }//end if/else
        */

        /*if (ni != null && ni.isAvailable()) {
            return true;
        } else {
            return false;
        }
        */

        return (ni != null && ni.isAvailable());

    }

    /*
     * And I got so pissed off at the bitch that I ripped off her panythose. After that, I
     * set the damn thing one fire.
     */
    private boolean isPhoneConnected(Context Context) {
        TelephonyManager tm =(TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        int networkType = tm.getNetworkType();

        switch (networkType) {
            case TelephonyManager.NETWORK_TYPE_LTE:
                return true;
            default:
                return false;
        }//end switch

        //return false;
    }

    //modify UI?
    protected void setAlarm() {
        //Toast.makeText(getApplicationContext(), "foo", Toast.LENGTH_SHORT ).show();
        Intent i = new Intent(AlarmClock.ACTION_SET_ALARM);
        i.putExtra(AlarmClock.EXTRA_HOUR, 15); //How long my boner goes with Viagra
        i.putExtra(AlarmClock.EXTRA_MINUTES, 5); //How long it goes without it -_-
        startActivity(i);
    }

    protected void setTextReminder() {

    }
    /*
     * Get page only on Friday's. Otherwise just use cached paged.
     * Attempt to do only one page request. Maybe.
     * Handle case when phone reboots?
     * Handle case when API is less than 24.
     * Handle case on Friday's if there is no internet connection.
     * User getSchedule onClickListener Toast to debug network connection?
     */
    @TargetApi(24)
    private void loadWebPage() {
        if (Build.VERSION.SDK_INT >= 24) {
            Calendar calendar = Calendar.getInstance();
            int day = calendar.get(Calendar.DAY_OF_WEEK);

            switch (day) {
                case Calendar.SATURDAY:
                case Calendar.SUNDAY:
                case Calendar.MONDAY:
                case Calendar.TUESDAY:
                case Calendar.WEDNESDAY:
                case Calendar.THURSDAY:
                    //getSchedule.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ONLY);
                    break;
                case Calendar.FRIDAY: //Load from network instead of cache
                    //getSchedule.loadUrl(TEST_URL); //Changed on 8 -14 - 2017
                    break;
            }//end switch
        }//end if.
    }//end loadWebPage()

    //Keep for debugging purposes
    public class WorkWebViewClient extends WebViewClient{

        /*@Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            authN = view.getUrl();
            handler.proceed("9857701@safeway.com", "");
        }
        */


        @TargetApi(21)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            super.shouldOverrideUrlLoading(view, request);
            /*if (Uri.parse(view.getUrl()).getHost().equals("myschedule.safeway.com")) {
                getSchedule.loadUrl("javascript:document.getElementsByName('EmpID').value = 'foo'");
                return false;
            }
            */
            //longinURL = view.getUrl();

            //getSchedule.loadUrl(""+view.getUrl());
            return false;
            //return true; //override ???
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            String s = "javascript:document.getElementByName('EmpID').value = 'foo'";
            //view.loadUrl("");
            //getWindow(); // added on 10 - 19 - 2017
            view.evaluateJavascript("javascript:document.getElementsByName('EmpID').value = 'foo'",
                    new ValueCallback<String>() {
                        @Override
                        public void onReceiveValue(String value) {
                        }

                    }) ;

        }//end onPageFinished()


        /*
         *Never gets called. 10 - 5- 2017.`
         */
        /*@Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
            //super.onReceivedHttpAuthRequest(view, handler, host, realm);
            //handler.proceed("", "");
            //String AuthN = view.getUrl();
            handler.proceed("9857701@safeway.com", "");
        }
        */


    } //end inner class
}//end MainActivity
